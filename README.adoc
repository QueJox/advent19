= Advent 2019

Pàgina web: https://quejox.gitlab.io/advent19/

== La mort del rei (3) OK

. Trobar una espasa (62987)
. Trobar un mapa (78510) 
. Espasa + mapa = 141497
. Trobar el rei -> https://quejox.gitlab.io/advent19/a44322871.html 
. Mata el rei travessant-li el cor ->https://quejox.gitlab.io/advent19/a74920174.html 
. Guanya *Rei de cors* -> https://quejox.gitlab.io/advent19/a09178352.html

Perdut pel bosc buscant el rei...
Dues coses et caldran, la primera trobar el camí i la segona una espasa per donar-li mort.

El rei és mort!!
Visca el rei!!

== El camí dels animals (4) OK

. Trobar elefant (23367)
. Trobar mussol (12427)
. Trobar la biblioteca
. mussol + elefant = (35794)
. Trobar  la porta dels animals -> https://quejox.gitlab.io/advent19/b75599214.html
. Guanya *Reina de trèvols* ->  https://quejox.gitlab.io/advent19/b54337768.html

 Darrera la porta...
 Pots obrir el paquet que ha deixat la reina de trèvols
 cor 39387
 
== Biblioteca OK 

Benvingut a la biblioteca
Una paraula mal posada espatlla el pensament més bell.

https://quejox.gitlab.io/advent19/z17296340.html
 
== El monòlit (3) OK 

. Trobar història
. Trobar plantilla (la clau és arboretum)
. Trobar el monòlit. Quina és la clau -> https://quejox.gitlab.io/advent19/d37099817.html
. Pots obrir el paquet que ha deixat el *rei de trèvols* -> https://quejox.gitlab.io/advent19/d41982008.html

== La reunió familiar (3)

. Trobar foto de la reunió familiar -> Descobrir els números que falten (7 piques, 5 cors, 4 trebols, 8 diamants)
. Trobar ordre dels pals
. Trobar el casino -> Rei de diamants

A sopar no han vingut
ningú els veu, s'han perdut?
em pregunto on seran
a veure si al casino, jugant.

https://quejox.gitlab.io/advent19/c29402243.html ->
https://quejox.gitlab.io/advent19/c40092271.html -> 


== El semàfor (4) OK 

. Bastonet groc
. Bastones vermell
. Bastonet taronja
. Plantilla
. Obrir candau amb combinació 3 dígits.


== Quant pesen dos mussols (3) OK

perquè l'alquímia funcioni
cal que no et tremoli el pols
et caldrà per preparar el beuratge
el què pesen dos mussols

. Trobar mussol 1.
. Trobar mussol 2.
. Trobar el laboratori https://quejox.gitlab.io/advent19/e91722283.html -> Obrir *Rei de diamants* https://quejox.gitlab.io/advent19/e99170002.html -> Bolet


L'alquimia ha funcionat
el que veus no és tot veritat
si el que vols és veure-ho tot 
caldrà mirar a l'interior


== El rellotge

. Foto del rellotge -> Hi ha la clau amagada

== La brúixola (3) OK

. Trobar equivalència runes.
. Trobar equivalència runes.
. Trobar amulet del vent.
. Obrir cadenat 4 xifres (7548).

== El naufragi 

. Trobar codi 1
. Trobar codi 2
. Trobar codi 3 (895)
. Trobar el tresor del naufragi https://quejox.gitlab.io/advent19/f18592043.html -> https://quejox.gitlab.io/advent19/f62330098.html

Quantes monedes té el tresor? (-- Obre la reina de Diamants

[plantuml, file="esquema.png"]
++++
@startmindmap
- root
-- (QP) La brúixola
--- (2P) Runes (1/2)
--- (3P) Runes (2/2)
--- (4P) Brúixola.
-- (KP) Quant pesen dos mussols
--- Bolet
---- (5P) Mussol 1
---- (6P) Mussol 2
---- (7P) Laboratori
-- (QC) El semàfor
--- (2C) Bastonet 1
--- (3C) Bastonet 2
--- (4C) Bastonet 3
--- (5C) Plantilla
-- (KC) La mort del rei
--- (6C) Espasa
--- (7C) Mapa
--- (8C) Rei
-- (QD) El naufragi
--- (2D) Trobar codi 1
--- (3D) Trobar codi 2
--- (4D) Trobar codi 3
--- (5D) Trobar el tresor del naufragi 
-- (KD)
--- (6D) Ordre dels pals
--- (7D) Casino
-- (QT) El camí dels animals
--- (2T) Elefant
--- (3T) Mussol
--- (4T) Biblioteca
--- (5T) Porta dels animals
-- (KT) El monòlit
--- (6T) Història
--- (7T) Plantilla
--- (8T) Monòlit

@endmindmap
++++

